﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Domain;
using IMDB.Repository;
namespace IMDB
{
    
    public class IMDBService
    {
        private MovieRepository _movieRepository = new MovieRepository();
        private ActorRepository _actorRepository = new ActorRepository();
        private ProducerRepository _producerRepository = new ProducerRepository();

        public void AddMovie(Movie newmovie)
        {
            
            if(string.IsNullOrEmpty(newmovie?.Title))
            {
                throw new CustomException("Title cant be null");
            }
            _movieRepository.Add(newmovie);
        }

        public void RemoveMovie(string name)
        {
            if(string.IsNullOrEmpty(name))
            {
                throw new CustomException("Title cant be null");
            }
            _movieRepository.Remove(name);
        }

        public List<Movie> GetMovies()
        {
            return _movieRepository.GetMovies();
        }

        public Movie GetMovie(string title)
        {
            var movie = _movieRepository.GetMovie(title);

            return movie;
        }
        ////////////////////////////////////////////////////////// ACTOR //////////////////////////////////


        public void AddActor(Actor newactor)
        {
            if (string.IsNullOrEmpty(newactor.Name))
            {
                throw new CustomException("ACtor cant be null");
            }
            _actorRepository.Add(newactor);
        }

        public Actor GetActor(string name)
        {
            var actor = _actorRepository.GetActor(name);
            if(actor == null)
            {
                throw new CustomException("No actor found");
            }
            return actor;
        }
        public List<Actor> GetActors()
        {
            return _actorRepository.GetActors();
        }


        ////////////////////// PRODUCER ///////////////////////////


        public List<Producer> GetProducers()
        {
            return _producerRepository.GetProducers();
        }

        public void AddProducer(Producer newproducer)
        {
            if(string.IsNullOrEmpty(newproducer.Name))
            {
                throw new CustomException("No producer found");
            }
            _producerRepository.Add(newproducer);
        }

        public Producer GetProducer(string name)
        {
            var producer = _producerRepository.GetProducer(name);
            if(producer == null)
            {
                throw new CustomException("No producer found");
            }
            return producer;
        }


    }
}
