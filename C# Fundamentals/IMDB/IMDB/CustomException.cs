﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB
{
    public class CustomException : Exception
    {
        
        public CustomException()
        {
        }

        public CustomException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public CustomException(string message) : base(message)
        {
        }
    }
}
