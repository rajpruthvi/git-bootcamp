﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB
{
    public class UnequalException : Exception
    {

        public UnequalException()
        {
        }

        public UnequalException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public UnequalException(string message) : base(message)
        {
        }
    }
}
