﻿using System;
using IMDB.Domain;
using IMDB.Repository;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
namespace IMDB
{
    class Program
    {
        static private IMDBService _service = new IMDBService();
        static void Main()
        {
            while (true)
            {
                try { 

                    Console.WriteLine("1. List movies \n2. Add Movie \n3. Add Actor \n4. Add Producer \n5. List Actor\n6. List Producer\n7. Delete Movie \n8. Exit");
                    int choice = int.Parse(Console.ReadLine());
                    switch (choice) {
                        case 1:
                            List<Movie> newmovies = _service.GetMovies();
                            if (newmovies.Count < 1)
                            {
                                Console.WriteLine("zero movies available");
                                break;
                            }
                            for (int i = 0; i < newmovies.Count; i++)
                            {
                                Console.WriteLine("Title: {0}\n", newmovies[i].Title);
                                Console.WriteLine("Year : {0}\n", newmovies[i].YearofRelease);
                                Console.WriteLine("Plot: {0}\n", newmovies[i].Plot);
                                for (int j = 0; j < newmovies[i].Actors.Count; j++)
                                    Console.WriteLine("Actor: {0}\n", newmovies[i].Actors[j].Name);
                                Console.WriteLine("Producer: {0}\n", newmovies[i].Producer.Name);
                            }
                            break;

                        case 2:
                            Movie movie = new Movie();
                            Console.WriteLine("To add a movie\n");
                            Console.WriteLine("enter movie title\n");
                            movie.Title = Console.ReadLine();
                            Console.WriteLine("enter movie year\n");
                            string yearOfRelease = Console.ReadLine();
                            if (Information.IsNumeric(yearOfRelease)) { 
                                movie.YearofRelease = int.Parse(yearOfRelease);
                            }
                            else {
                                Console.WriteLine("enter an interger for year");
                                yearOfRelease = Console.ReadLine();
                                movie.YearofRelease = int.Parse(yearOfRelease);
                            }
                            Console.WriteLine("enter plot\n");
                            movie.Plot = Console.ReadLine();

                            List<Actor> getActors = _service.GetActors();
                            List<Actor> addActors = new List<Actor>();
                            if (getActors.Count > 0)
                            {
                                Console.WriteLine("choose actor\n");
                                for (int i = 0; i < getActors.Count; i++)
                                {
                                    Console.WriteLine(i+1 + " " + getActors[i].Name + "\t");
                                }
                                string getActorsNumbers = Console.ReadLine();
                            
                                var numbers = getActorsNumbers.Split(' ').Select(Int32.Parse).ToList();
                                for (int i = 0; i < numbers.Count; i++)
                                {
                                    addActors.Add(new Actor() { Name = getActors[numbers[i] - 1].Name, DOB = getActors[numbers[i] - 1].DOB });
                                }
                                movie.Actors = addActors;
                            }
                            else
                            {
                                Console.WriteLine("actors dont exist, cant add movie");
                                break;
                            }

                            List<Producer> getProducer = _service.GetProducers();
                            Producer addProducer = new Producer();
                            if (getProducer.Count > 0)
                            {
                                Console.WriteLine("Choose producer");
                                for (int i = 0; i < getProducer.Count; i++)
                                {
                                    Console.WriteLine(i + 1 + " " + getProducer[i].Name + "\t");
                                }
                                Console.WriteLine("enter producer number");
                                int num = int.Parse(Console.ReadLine());
                                if (num < 0 || num > getProducer.Count)
                                {
                                    throw new CustomException("invalid producer");
                                }
                                addProducer.Name = getProducer[num - 1].Name;
                                addProducer.DOB = getProducer[num - 1].DOB;
                                movie.Producer = addProducer;
                            } else
                            {
                                Console.WriteLine("producers dont exist, cant add movie");
                                break;
                            }
                            _service.AddMovie(movie);
                            Console.WriteLine("added a movie");
                            break;

                        case 3:
                            Console.WriteLine("To add Actor\n");
                            Console.WriteLine("Enter name and dob\n");
                            Actor actor = new Actor();
                            actor.Name = Console.ReadLine();
                            actor.DOB = Console.ReadLine();
                            _service.AddActor(actor);
                            Console.WriteLine("added an actor");
                            break;
                        case 4:
                            Console.WriteLine("to add producer\n");
                            Console.WriteLine("enter name and age\n");
                            Producer producer = new Producer();
                            producer.Name = Console.ReadLine();
                            producer.DOB = Console.ReadLine();
                            _service.AddProducer(producer);
                            Console.WriteLine("added a producer");
                            break;
                        case 5:
                            List<Actor> getAllActors = _service.GetActors();

                            if (getAllActors.Count > 0)
                            {
                                Console.WriteLine("actors\n");
                                for (int i = 0; i < getAllActors.Count; i++)
                                {
                                    Console.WriteLine(i+1 + ". " + getAllActors[i].Name + " " + getAllActors[i].DOB + "\t");
                                }
                            } else
                            {
                                Console.WriteLine("zero actors");
                                break;
                            }
                            break;
                        case 6:
                            List<Producer> getAllProducer = _service.GetProducers();
                            if (getAllProducer.Count < 1)
                            {
                                Console.WriteLine("zero producer");
                            }
                            else
                            {
                                Console.WriteLine("Producers: \n");
                                for (int i = 0; i < getAllProducer.Count; i++)
                                {
                                    Console.WriteLine(i+1 + ". " + getAllProducer[i].Name + " " + getAllProducer[i].DOB + "\t");
                                }
                            }
                            break;
                        case 7:
                            Console.WriteLine("enter movie name to delete");
                            string name = Console.ReadLine().Trim();
                            List<Movie> allMovies = _service.GetMovies();                        
                            if (allMovies.Count < 1)
                            {
                                Console.WriteLine("zero movies available");
                                break;
                            }
                            if (_service.GetMovie(name) == null)
                            {
                                Console.WriteLine("movie don't exist");
                                break;
                            }
                            _service.RemoveMovie(name);
                            Console.WriteLine("movie deleted successfully");
                            break;
                        case 8:
                            return;
                        default:
                            Console.WriteLine("wrong choice entered, choose again");
                            break;


                    }
                
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

        }
    }
}
