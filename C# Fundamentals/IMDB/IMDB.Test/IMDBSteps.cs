﻿using System;
using IMDB.Domain;
using Newtonsoft.Json;
using TechTalk.SpecFlow;
using System.Collections.Generic;
using TechTalk.SpecFlow.Assist;
using Xunit;


namespace IMDB.Test
{
    [Binding]
    public class IMDBSteps
    {
        private IMDBService _service = new IMDBService();
        private readonly ScenarioContext scenarioContext;
        

        Movie movie = new Movie();
        Actor actor = new Actor();
        Producer producer = new Producer();
        
        
        string movieTitle = "";
        string actorName = "";
        string producerName = "";


        public IMDBSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
        }

        [Given(@"I have the list of movies")]
        static public void GivenIHaveTheListOfMovies()
        {
                
        }
        
        [Given(@"I have a movie ""(.*)""")]
        public void GivenIHaveAMovie(string title)
        {
            movie.Title = title;
            movieTitle = title;
        }
        
        [Given(@"year of release is ""(.*)""")]
        public void GivenYearOfReleaseIs(int year)
        {
            movie.YearofRelease = year;
        }
        
        [Given(@"plot is ""(.*)""")]
        public void GivenPlotIs(string plot)
        {
            movie.Plot = plot;
        }
        
        [When(@"I retrieve the list of movies")]
        public void WhenIRetrieveTheListOfMovies()
        {
            var movies = _service.GetMovies();
            scenarioContext.Add("movies", movies);
        }
        
        [When(@"I add the movie")]
        public void WhenIAddTheMovie()
        {
            _service.AddMovie(movie);
        }

        [Then(@"the list should look like")]
        public void ThenTheListShouldLookLike(Table table)
        {
            var movies = _service.GetMovies();

            List<Movie> getMovie = new List<Movie>();
            for(int i = 0; i < movies.Count; i++)
            {
                Movie m = new Movie();
                var name = table.Rows[i]["Actors"];
                var producerName = table.Rows[i]["Producer"];
                m.Actors.Add(new Actor() { Name = name, DOB = "DDMMYYYY" });
                m.Producer = new Producer() { Name = producerName, DOB = "DDMMYYYY" };
                m.YearofRelease = int.Parse(table.Rows[i]["Year of Release"]);
                m.Plot = table.Rows[i]["Plot"];
                m.Title = table.Rows[i]["Title"];
                getMovie.Add(m);
            }

            for (int i = 0; i < movies.Count; i++) {
                if ( movies[i].YearofRelease != getMovie[i].YearofRelease || 
                    movies[i].Plot != getMovie[i].Plot || movies[i].Producer.Name != getMovie[i].Producer.Name ||
                    movies[i].Title != getMovie[i].Title) {
                    throw new UnequalException("movies dont match");
                }
            }

        }
        
        [Then(@"the list should be updated like")]
        public void ThenTheListShouldBeUpdatedLike(Table table)
        {
            var movies = _service.GetMovie(movieTitle);
            var actor = table.Rows[0]["Actors"];
            var year = int.Parse(table.Rows[0]["Year of Release"]);
            var plot = table.Rows[0]["Plot"];
            var producer = table.Rows[0]["Producer"];
            var title = table.Rows[0]["Title"];

            if(movies.Actors[0].Name != actor || movies.Plot != plot || movies.Producer.Name != producer || movies.Title != title || movies.YearofRelease != year)
            {
                throw new UnequalException("comparision failed, doens't match");
            } 

        }





        [Given(@"actors are ""(.*)""")]
        public void GivenActorsAre(string actorString)
        {

            List<string> newactors = new List<string>(actorString.Split(','));
            List<Actor> actor = new List<Actor>();
            foreach(var act in newactors)
            {
                actor.Add(new Actor { Name = act, DOB = "DDMMYYYY" });        
            }
            movie.Actors= actor;

        }

        [Given(@"producer of movie is ""(.*)""")]
        public void GivenProducerOfMovieIs(string producer)
        {

            movie.Producer =  new Producer() { Name = producer, DOB = "DDMMYYYY"};
        }


        [Given(@"producer is ""(.*)""")]
        public void GivenProducerIs(string name)
        {
            producer.Name = name;
            producerName = name;
        }


        [Given(@"date of birth of producer""(.*)""")]
        public void GivenDateOfBirthOfProducer(string dob)
        {
            producer.DOB = dob;
        }



        [Given(@"date of birth ""(.*)""")]
        public void GivenDateOfBirth(string dob)
        {
            actor.DOB = dob;
        }

        [When(@"I add the producer")]
        public void WhenIAddTheProducer()
        {
            _service.AddProducer(producer);

        }

        [Then(@"the producer list should be updated like")]
        public void ThenTheProducerListShouldBeUpdatedLike(Table table)
        {

            var producers = _service.GetProducer(producerName);
            table.CompareToInstance<Producer>(producers);
        }

        [Given(@"I have an actor ""(.*)""")]
        public void GivenIHaveAnActor(string name)
        {
            actor.Name = name;
            actorName = name;
        }

        [When(@"I add the actor")]
        public void WhenIAddTheActor()
        {
            _service.AddActor(actor);
        }

        [Then(@"the actor list should be updated like")]
        public void ThenTheActorListShouldBeUpdatedLike(Table table)
        {
            var actors = _service.GetActor(actorName);
            table.CompareToInstance<Actor>(actors);
        }


        [Given(@"I have a movie in database named ""(.*)""")]
        public void GivenIHaveAMovieInDatabaseNamed(string movieName)
        {
            movieTitle = movieName;
        }

        [When(@"I delete the movie")]
        public void WhenIDeleteTheMovie()
        {
            _service.RemoveMovie(movieTitle);
        }

        [Then(@"the movie should be removed from the database like")]
        public void ThenTheMovieShouldBeRemovedFromTheDatabaseLike(Table table)
        {
            if (_service.GetMovie(movieTitle) != null)
            {
                throw new UnequalException("movie didn't get deleted");
            }
        }



        [BeforeScenario("mytag")]
        public void AddTestData()
        {
            _service.GetMovies().Clear();
            _service.AddMovie(new Movie()
            {
                Title = "Titanic",
                YearofRelease = 1997,
                Plot = "A lot of people take the Ice Bucket Challenge. It doesn’t end well.",
                Actors = new List<Actor>() { new Actor() { Name = "Leonardo DiCaprio", DOB = "" } },
                Producer = new Producer() { Name = "James Cameron", DOB = "DDMMYYYY" }

            }); 
            _service.AddMovie(new Movie()
            {
                Title = "Ready Player Run",
                YearofRelease = 2018,
                Plot = "People play video game for a grand prize",
                Actors = new List<Actor>() { new Actor() { Name = "Leonardo DiCaprio", DOB = "" } },
                Producer = new Producer() { Name = "Steven Spielberg", DOB = "DDMMYYYY" }
            });
        }
    }
}
