﻿Feature: IMDB
	In order to keep track of movies 
	I need a system to do that for me
	And show me all the movies along with actors and producers

@mytag
Scenario: List movie
	Given I have the list of movies
	When I retrieve the list of movies
	Then the list should look like
	| Title            | Year of Release | Plot                                                                | Actors            | Producer         |
	| Titanic          | 1997            | A lot of people take the Ice Bucket Challenge. It doesn’t end well. | Leonardo DiCaprio | James Cameron    |
	| Ready Player Run | 2018            | People play video game for a grand prize                            | Tye Sheridan      | Steven Spielberg |

Scenario: Add movie
	Given I have a movie "Ready Player Run"
	And year of release is "2018"
	And plot is "People play video game for a grand prize"
	And actors are "Tye Sheridan"
	And producer of movie is "Steven Spielberg"
	When I add the movie
	Then the list should be updated like
	| Title            | Year of Release | Plot                                     | Actors       | Producer         |
	| Ready Player Run | 2018            | People play video game for a grand prize | Tye Sheridan | Steven Spielberg |

@addproducer
Scenario: Add producer
	Given producer is "Steven Spielberg"
	And date of birth of producer"18/12/1946"
	When I add the producer
	Then the producer list should be updated like 
	| Name         | DOB        |
	| Steven Spielberg | 18/12/1946 |


@addactor
Scenario: Add actor
	Given I have an actor "Tye Sheridan"
	And date of birth "11/11/1996"
	When I add the actor
	Then the actor list should be updated like
	| Name         | DOB        |
	| Tye Sheridan | 11/11/1996 |


@deletemovie
Scenario: Delete movie
	Given I have a movie in database named "Ready Player Run"
	When I delete the movie
	Then the movie should be removed from the database like
	| Title            | Year of Release | Plot                                                                | Actors            | Producer         |
	| Titanic          | 1997            | A lot of people take the Ice Bucket Challenge. It doesn’t end well. | Leonardo DiCaprio | James Cameron    |

