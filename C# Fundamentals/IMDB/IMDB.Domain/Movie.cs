﻿using System;
using System.Collections.Generic;

namespace IMDB.Domain
{
    public class Movie
    {

        public Movie()
        {
            Actors = new List<Actor>();
        }

        public string Title { get; set; }
        public int YearofRelease { get; set; }
        public string Plot { get; set; }
        public List<Actor> Actors { get; set; }
        public Producer Producer { get; set; }
    }
}
