﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Person
    {
        public string Name { get; set; }

        public string DOB { get; set; }
    }
}
