﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Domain;
using System.Linq;

namespace IMDB.Repository
{
    public class ActorRepository 
    {
        private List<Actor> _actors = new List<Actor>();

        public void Add(Actor newActor)
        {
            _actors.Add(newActor);
        }

        public List<Actor> GetActors()
        {
            return _actors;
        }
        public Actor GetActor(string name)
        {
            return _actors.FirstOrDefault(a => a.Name == name);
        }
    }
}
