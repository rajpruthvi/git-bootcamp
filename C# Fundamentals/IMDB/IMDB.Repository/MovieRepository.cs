﻿using System;
using IMDB.Domain;
using System.Collections.Generic;
using System.Linq;
namespace IMDB.Repository
{
    public class MovieRepository
    {
        private List<Movie> _movies = new List<Movie>();

        public void Add(Movie newMovie)
        {
            _movies.Add(newMovie);
        }
            
        public List<Movie> GetMovies()
        {
            return _movies;
        }
        public Movie GetMovie(string title)
        {
            return _movies.FirstOrDefault(m => m.Title == title);
        }

        public void Remove(string title)
        {
            Movie getMovie = new Movie();
            getMovie = _movies.FirstOrDefault(m => m.Title == title);
            _movies.Remove(getMovie);
        }
    }
}
