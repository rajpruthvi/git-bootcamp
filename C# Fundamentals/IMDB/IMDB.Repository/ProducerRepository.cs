﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Domain;
using System.Linq;
namespace IMDB.Repository
{
    public class ProducerRepository
    {
        private List<Producer> _producer = new List<Producer>();
        
        public void Add(Producer newProducer)
        {
            _producer.Add(newProducer);
        }


        public List<Producer> GetProducers()
        {
            return _producer;
        }

        public Producer GetProducer(string name)
        {
            return _producer.FirstOrDefault(p => p.Name == name);
        }

    }
}
