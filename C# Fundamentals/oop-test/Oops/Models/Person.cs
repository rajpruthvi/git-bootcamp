﻿using System.Collections.Generic;

namespace Oops.Models
{
    public class Person
    {

        public Person()
        {
            Attendance = new List<Attendance>();
        }
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Attendance> Attendance { get; set; }
    }
}
