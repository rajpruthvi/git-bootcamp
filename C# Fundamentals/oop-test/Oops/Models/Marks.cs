﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oops.Models
{
    public class Marks
    {
        public string Subject { get; set; }

        public int Mark { get; set; }
    }
}
