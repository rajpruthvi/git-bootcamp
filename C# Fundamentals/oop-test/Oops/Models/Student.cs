﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oops.Models
{
    public class Student : Person
    {
        public Student()
        {
            marks = new List<Marks>();
        }

        public List<Marks> marks { get; set; }
    }
}
