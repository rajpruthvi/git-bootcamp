﻿using System;
using System.Collections.Generic;
using System.Text;
using Oops.Repositories;
using Oops.Models;
namespace Oops.Services
{
    public class PersonService : IPersonService
    {

        private PersonRepository _personrepository = new PersonRepository();
        public void MarkAttendance(Person person, Attendance attendance)
        {
            if(person.Id < 0)
            {
                throw new Exception("person dont exist");
            }
            person.Attendance.Add(attendance);
        }

        public List<Attendance> GetAttendance(int id)
        {
            var person = _personrepository.GetPerson(id);
            if(person == null)
            {
                throw new Exception("person dont exist");
            }
            return person.Attendance;
        }


        public void AddPerson(Person person)
        {
            if (person.Id == 0 || person.Name == null)
            {
                throw new Exception("student cant have null values");
            }
            _personrepository.Add(person);
        }

        public Person GetPerson(int id)
        {
            return _personrepository.GetPerson(id);
        }

        public List<Person> GetPeople()
        {
            return _personrepository.GetPeople();
        } 
    }
}
