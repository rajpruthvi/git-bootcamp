﻿using System;
using Oops.Services;
using Oops.Models;
using System.Collections.Generic;
namespace Oops
{
    class Program
    {
        static private PersonService _personService = new PersonService();
        static void Main(string[] args)
        {

            while (true)
            {
                try
                {
                    Console.WriteLine("choose 1. Add Student\n 2.Display Student\n 3.Exit\n");
                    int choice = int.Parse(Console.ReadLine());

                    switch (choice)
                    {
                        case 1:
                            Student student = new Student();
                            Console.WriteLine("enter name\n");
                            student.Name = Console.ReadLine();
                            Console.WriteLine("enter id\n");
                            student.Id = int.Parse(Console.ReadLine());
                            Console.WriteLine("enter subject and marks");
                            student.marks = new List<Marks>() { new Marks() { Subject = Console.ReadLine(), Mark = int.Parse(Console.ReadLine()) } };
                            student.Attendance = new List<Attendance>() { new Attendance() { Date = DateTime.Parse("02-02-2002"), Present = true } };
                            _personService.AddPerson(student);
                            break;

                        case 2:
                            var getStudent = _personService.GetPeople();

                            for (int i = 0; i < getStudent.Count; i++)
                            {
                                Console.WriteLine("Student: {0}", i + 1);
                                Console.WriteLine("Name: {0}", getStudent[i].Name);
                                Console.WriteLine("Id: {0}", getStudent[i].Id);
                            }
                            Console.WriteLine("ENTER student number");
                            int studentIndex = int.Parse(Console.ReadLine());

                            if (studentIndex > getStudent.Count)
                            {
                                Console.WriteLine("student dont exist");
                                break;
                            }
                            var studentAttendance = getStudent[studentIndex - 1].Attendance;
                            for (int i = 0; i < studentAttendance.Count; i++)
                            {
                                Console.WriteLine("Date: {0}\n", studentAttendance[i].Date);
                                Console.WriteLine("Attendance: {0}\n", studentAttendance[i].Present);
                            }
                            break;

                        case 3:
                            return;
                        default:
                            Console.WriteLine("wrong choice entered, please try again");
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
