﻿using System;
using TechTalk.SpecFlow;
using Oops.Models;
using Oops.Services;
using System.Linq;
using System.Collections.Generic;
using Xunit;


namespace Oops.Test
{
    [Binding]
    public class AttendanceSteps
    {

        private PersonService _personService = new PersonService();
        Person person = new Person();
        Attendance attendance = new Attendance();
        string todaydate;
        private readonly ScenarioContext scenarioContext;

        public AttendanceSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
        }


        [Given(@"I have a student with Id as ""(.*)""")]
        public void GivenIHaveAStudentWithIdAs(int id)
        {
            person = _personService.GetPerson(id);    
            scenarioContext.Add("personId", id);
        }

        [When(@"When I mark the student as present for ""(.*)""")]
        public void WhenWhenIMarkTheStudentAsPresentFor(string date)
        {
            
            person.Attendance.Add(new Attendance() { Date = DateTime.Parse(date), Present = true });            
            _personService.AddPerson(person);
            scenarioContext.Add("todayDate", date);
        }

        [Then(@"the attendance for the student for the day should be ""(.*)""")]
        public void ThenTheAttendanceForTheStudentForTheDayShouldBe(string checkBool)
        {
            var getId = int.Parse(scenarioContext["personId"].ToString());
            var getPerson = _personService.GetPerson(getId);
            var date = DateTime.Parse(scenarioContext["todayDate"].ToString());
            var getDate = (from p in getPerson.Attendance
                            where p.Date == date
                            select p).FirstOrDefault(s => s.Present == bool.Parse(checkBool));
            Assert.Equal(getDate.Present, Boolean.Parse(checkBool));
            
        }

        [BeforeScenario("scene1")]
        public void AddTestData()
        {
            Attendance attendance = new Attendance();
            person = new Student()
            {
                Id = 1251515,
                Name = "asd",   
                Attendance = new List<Attendance>()
                { new Attendance() { Date = DateTime.Parse("09-01-2020"), Present = true }
                },
                marks = new List<Marks>() { new Marks() { Subject = "English", Mark = 88 } }
                
            };
            _personService.AddPerson(person);
        }
    }
}
